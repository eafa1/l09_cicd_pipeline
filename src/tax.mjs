/**
 * Class to handle VAT(Value Added Tax)
 */
export default class Tax {
    /**
     * Apply tax to the list price.
     * @param {number} list_price before tax
     * @param {number} tax in percentage
     * @returns {number} after tax
     */
    static applyTax(list_price, tax) {
        const tax_rate = tax / 100;
        const after_tax = list_price + list_price * tax_rate;
        return after_tax;
    }
    /**
     * Deduct tax from the after_tax price
     * @param {number} total_price after tax 
     * @param {number} tax in percentage
     * @returns {number} before tax
     */
    static deductTax(total_price, tax) {
        const tax_rate = 1 + (tax / 100);
        const list_price = total_price / tax_rate;
        return list_price;
    }
    /**
     * Calculate tax rate based on list_price and total_price
     * @param {number} list_price no tax
     * @param {number} total_price tax included
     * @returns {number} tax rate percentage
     */
    static figureTaxRate(list_price, total_price) {
        const tax_rate = (1 / (list_price / total_price)) - 1;
        const tax_rate_percent = tax_rate * 100;
        return tax_rate_percent;
    }
}
